/**
 * @file	ble.c
 *
 * @brief	Bluetooth Low Energy protocol
 *
 * @author	Christian Halter
 * @author	Maximiliano Leikan
 * @author	Denis Genero
 *
 * @version	0.0
 *
 * @date	06/12/2018
 *
 * Version history
 * |   Version   |    Date    | Description                     | Author            |
 * |:-----------:|:----------:|:-------------------------------:|:------------------|
 * |     0.0     | 06/12/2018 | Initial version                 | Christian Halter  |
 *
 */

/*==================[inclusions]=============================================*/
#include "ble.h"
#include "bool.h"

/*==================[macros and definitions]=================================*/

/**
 * @brief	Standard GATT characteristic properties.
 */
#define	GATT_CHAR_PROP_NONE							0x00	/**< None characteristic property available. */
#define	GATT_CHAR_PROP_BROADCAST					0x01	/**< Permits broadcasts of the characteristic value using the Server Characteristic Configuration descriptor. */
#define	GATT_CHAR_PROP_READ							0x02	/**< Permits reads of the characteristic value. */
#define	GATT_CHAR_PROP_WRITE_WITHOUT_RESPONSE		0x04	/**< Permits writes of the characteristic value without response. */
#define	GATT_CHAR_PROP_WRITE						0x08	/**< Permits writes of the characteristic value with response. */
#define	GATT_CHAR_PROP_NOTIFY						0x10	/**< Permits notifications of a characteristic value without acknowledgment. */
#define	GATT_CHAR_PROP_INDICATE						0x20	/**< Permits indications of a characteristic value with acknowledgment. */
#define	GATT_CHAR_PROP_AUTHENTICATED_SIGNED_WRITES	0x40	/**< Permits signed writes to the characteristic value. */
#define	GATT_CHAR_PROP_EXTENDED_PROPERTIES			0x80 	/**< Additional characteristic properties are defined in the Characteristic Extended Properties descriptor */

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

uint8_t CharToInteger(char c)
{
	if ((c >= '0') && (c <= '9'))
	{
		return c - '0';
	}
	else if ((c >= 'a') && (c <= 'f'))
	{
		return c - 'a' + 10;
	}
	else if ((c >= 'A') && (c <= 'F'))
	{
		return c - 'A' + 10;
	}
	else
	{
		return 0;
	}
}


//UUIDnonstd_t ReadUUID(const char* string_uuid)
//{
//
//}



