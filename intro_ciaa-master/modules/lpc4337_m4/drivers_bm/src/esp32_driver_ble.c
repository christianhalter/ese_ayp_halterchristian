/**
 * @file	esp32_driver_ble.c
 *
 * @brief	ESP32 Bluetooth Low Energy protocol
 *
 * @author	Christian Halter
 * @author	Maximiliano Leikan
 * @author	Denis Genero
 *
 * @version	0.0
 *
 * @date	06/12/2018
 *
 * Version history
 * |   Version   |    Date    | Description                     | Author            |
 * |:-----------:|:----------:|:-------------------------------:|:------------------|
 * |     0.0     | 06/12/2018 | Initial version                 | Christian Halter  |
 *
 */

/*==================[inclusions]=============================================*/
#include "esp32_driver_ble.h"

/*==================[macros and definitions]=================================*/

#define START_SERVER		1	/**< Start server command.*/
#define STOP_SERVER			2	/**< Stop server command.*/
#define START_ADVERTISING	3	/**< Start advertising command.*/
#define STOP_ADVERTISING	4	/**< Stop advertising command.*/
#define CHECK_CONNECTION	5	/**< Connection verify command.*/
#define READ_CHAR_VALUE		6	/**< Read characteristic value command.*/
#define WRITE_CHAR_VALUE	7	/**< Write characteristic value command.*/
#define REFRESH_CHAR_VALUE	8	/**< Refresh characteristic value command.*/

#define IS_CONNECTED		1	/**< Connection verified answer.*/
#define IS_NOT_CONNECTED	0	/**< No connection answer.*/

#define START_CHAR		'<'
#define END_CHAR		'>'
#define SEPARATOR		','

/*==================[internal data declaration]==============================*/

static ESP32frame_t rx_data;
static uint8_t data_received_flag = 0;
static uint8_t refresh_flag = 0;

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


void ESP32SendCommand(uint8_t request, uint8_t uuid, uint8_t value)
{
	ESP32frame_t sending;
	sending.command = (request+'0');
	sending.short_UUID = (uuid + '0');
	sending.value = (value+'0');
	RS232SendChar(sending.start_marker);
	RS232SendChar(sending.command);
	RS232SendChar(sending.first_separator);
	RS232SendChar(sending.short_UUID);
	RS232SendChar(sending.second_separator);
	RS232SendChar(sending.value);
	RS232SendChar(sending.end_marker);
}

void ESP32StartBLEServer()
{
	ESP32SendCommand(START_SERVER, 0, 0);
}

void ESP32StopBLEServer()
{
	ESP32SendCommand(STOP_SERVER, 0, 0);
}

void ESP32StartAdvertising()
{
	ESP32SendCommand(START_ADVERTISING, 0, 0);
}

void ESP32StopAdvertising()
{
	ESP32SendCommand(STOP_ADVERTISING, 0, 0);
}

ESP32frame_t ESP32ReceiveFrame()
{
	ESP32frame_t frame_rx;
	frame_rx.start_marker=RS232ReadBlk();
	frame_rx.command=RS232ReadBlk();
	frame_rx.first_separator=RS232ReadBlk();
	frame_rx.short_UUID=RS232ReadBlk();
	frame_rx.second_separator=RS232ReadBlk();
	frame_rx.value=RS232ReadBlk();
	frame_rx.end_marker=RS232ReadBlk();
	return frame_rx;
}

bool ESP32IsConnected()
{
	ESP32frame_t rx;
	ESP32SendCommand(CHECK_CONNECTION,0,0);
	rx=ESP32ReceiveFrame();
	if (rx.value == IS_CONNECTED)
	{
		return true;
	}
	else {}
	if (rx.value == IS_NOT_CONNECTED)
	{
		return false;
	}
	else {}
}

void ESP32WriteCharacteristic(characteristic_t characteristic)
{
	ESP32SendCommand(WRITE_CHAR_VALUE,characteristic.short_UUID,characteristic.value);
}

void ESP32ReadCharacteristicValue(uint8_t uuid)
{
	ESP32SendCommand(READ_CHAR_VALUE, uuid, 0);
}

characteristic_t ESP32RefreshCharacteristic(ESP32frame_t rx)
{
	characteristic_t characteristic;
	characteristic.short_UUID = rx.short_UUID;
	characteristic.value = rx.value;
	return characteristic;
}

void ESP32Init()
{
	RS232Init();
	RS232IntEnable(ESP32ReadByte);
}

void ESP32ReadByte()
{
	static uint8_t rx_byte = 0;
	static uint8_t rx_command_flag = 0;
	static uint8_t rx_first_separator_flag = 0;
	static uint8_t rx_uuid_flag = 0;
	static uint8_t rx_second_separator_flag = 0;
	static uint8_t rx_value_flag = 0;
	static uint8_t rx_end_flag = 0;
	rx_byte = RS232Read();
	if (rx_command_flag)
	{
		if (rx_first_separator_flag)
		{
			if (rx_uuid_flag)
			{
				if (rx_second_separator_flag)
				{
					if (rx_value_flag)
					{
						if (rx_end_flag)
						{
							if (rx_byte == END_CHAR)
							{
								rx_data.end_marker = rx_byte;
								data_received_flag = 1;
								rx_command_flag = 0;
								rx_first_separator_flag = 0;
								rx_uuid_flag = 0;
								rx_second_separator_flag = 0;
								rx_value_flag = 0;
								rx_end_flag = 0;
								if (rx_data.command == REFRESH_CHAR_VALUE)
								{
									refresh_flag = 1;
								}
							}
							else
							{
								data_received_flag = 0;
								rx_command_flag = 0;
								rx_first_separator_flag = 0;
								rx_uuid_flag = 0;
								rx_second_separator_flag = 0;
								rx_value_flag = 0;
								rx_end_flag = 0;
							}
						}
						rx_data.value = rx_byte;
						rx_end_flag = 1;
					}
					if (rx_byte == SEPARATOR)
					{
						rx_data.second_separator = rx_byte;
						rx_value_flag = 1;
					}
					else
					{
						data_received_flag = 0;
						rx_command_flag = 0;
						rx_first_separator_flag = 0;
						rx_uuid_flag = 0;
						rx_second_separator_flag = 0;
						rx_value_flag = 0;
						rx_end_flag = 0;
					}
				}
				else
				{
					rx_data.short_UUID = rx_byte;
					rx_second_separator_flag = 1;
				}
			}
			else
			{
				if (rx_byte == SEPARATOR)
				{
					rx_data.first_separator = rx_byte;
					rx_uuid_flag = 1;
				}
				else
				{
					data_received_flag = 0;
					rx_command_flag = 0;
					rx_first_separator_flag = 0;
					rx_uuid_flag = 0;
					rx_second_separator_flag = 0;
					rx_value_flag = 0;
					rx_end_flag = 0;
				}
			}
		}
		else
		{
			rx_data.command = rx_byte;
			rx_first_separator_flag = 1;
		}
	}
	else
	{
		if (rx_byte == START_CHAR)
		{
			rx_data.start_marker=rx_byte;
			rx_command_flag = 1;
			data_received_flag = 0;
		}
	}
}




/*==================[end of file]============================================*/
