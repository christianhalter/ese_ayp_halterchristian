/**
 * @file	esp32_driver_ble.h
 *
 * @brief	ESP32 Bluetooth Low Energy protocol
 *
 * @author	Christian Halter
 * @author	Maximiliano Leikan
 * @author	Denis Genero
 *
 * @version	0.0
 *
 * @date	06/12/2018
 *
 * Version history
 * |   Version   |    Date    | Description                  | Author            |
 * |:-----------:|:----------:|:----------------------------:|:------------------|
 * |     0.0     | 06/12/2018 | Initial version              | Christian Halter  |
 *
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_BM_INC_ESP32_DRIVER_BLE_H_
#define MODULES_LPC4337_M4_DRIVERS_BM_INC_ESP32_DRIVER_BLE_H_

/*==================[inclusions]=============================================*/
#include "stdint.h"
#include "bool.h"

/*==================[macros]=================================================*/


/*==================[typedef]================================================*/

typedef struct{
	uint8_t start_marker = '<';
	uint8_t command;
	uint8_t first_separator = ',';
	uint8_t short_UUID;
	uint8_t second_separator = ',';
	uint8_t value;
	uint8_t end_marker = '>';
}ESP32frame_t;

typedef struct{
	uint8_t short_UUID;
	uint8_t value;
}characteristic_t;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/**
 * @brief Send command to ESP32
 *
 * This function sends a command to the ESP32 connected to the RS232 pins.
 *
 * @param command	command to be sent
 */
void ESP32SendCommand(uint8_t command);

/**
 * @brief Start BLE server
 *
 * Sends a command to start the ESP32 device as a BLE server.
 */
void ESP32StartBLEServer();

/**
 * @brief Stop BLE server
 *
 * Sends a command to stop the BLE server of the ESP32 device.
 */
void ESP32StopBLEServer();

/**
 * @brief Start BLE data advertising
 *
 * Sends a command to start the data advertising in a BLE active server of an ESP32 device
 */
void ESP32StartAdvertising();

/**
 * @brief Stop BLE data advertising
 *
 * Sends a command to stop the data advertising in a BLE active server of an ESP32 device
 */
void ESP32StopAdvertising();

/**
 * @brief Check BLE active connection
 *
 * Sends a command to the BLE server of an ESP32 to see if it has an active connection
 *
 * @return	TRUE if the device has an active connection, FALSE if not.
 */
bool ESP32IsConnected();

/**
 * @brief Write characteristic function
 *
 * Sends a command to the BLE server of an ESP32 to change a characteristic's value.
 *
 * @param characteristic	characteristic to be written.
 */
void ESP32WriteCharacteristic(characteristic_t characteristic);

/**
 * @brief Read characteristic function
 *
 *
 */
void ESP32ReadCharacteristicValue(uint8_t uuid);


/*==================[end of file]============================================*/
#endif /* MODULES_LPC4337_M4_DRIVERS_BM_INC_ESP32_DRIVER_BLE_H_ */
