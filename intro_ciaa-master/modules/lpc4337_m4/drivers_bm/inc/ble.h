/**
 * @file	ble.h
 *
 * @brief	Bluetooth Low Energy protocol
 *
 * @author	Christian Halter
 * @author	Maximiliano Leikan
 * @author	Denis Genero
 *
 * @version	0.0
 *
 * @date	06/12/2018
 *
 * Version history
 * |   Version   |    Date    | Description                     | Author            |
 * |:-----------:|:----------:|:-------------------------------:|:------------------|
 * |     0.0     | 06/12/2018 | Initial version                 | Christian Halter  |
 *
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_BM_INC_BLE_H_
#define MODULES_LPC4337_M4_DRIVERS_BM_INC_BLE_H_

/*==================[inclusions]=============================================*/

#include "stdint.h"

/*==================[macros and definitions]=================================*/

/**
 * @brief	Standard GATT characteristic properties.
 */
#define	GATT_CHAR_PROP_NONE							0x00	/**< None characteristic property available. */
#define	GATT_CHAR_PROP_BROADCAST					0x01	/**< Permits broadcasts of the characteristic value using the Server Characteristic Configuration descriptor. */
#define	GATT_CHAR_PROP_READ							0x02	/**< Permits reads of the characteristic value. */
#define	GATT_CHAR_PROP_WRITE_WITHOUT_RESPONSE		0x04	/**< Permits writes of the characteristic value without response. */
#define	GATT_CHAR_PROP_WRITE						0x08	/**< Permits writes of the characteristic value with response. */
#define	GATT_CHAR_PROP_NOTIFY						0x10	/**< Permits notifications of a characteristic value without acknowledgment. */
#define	GATT_CHAR_PROP_INDICATE						0x20	/**< Permits indications of a characteristic value with acknowledgment. */
#define	GATT_CHAR_PROP_AUTHENTICATED_SIGNED_WRITES	0x40	/**< Permits signed writes to the characteristic value. */
#define	GATT_CHAR_PROP_EXTENDED_PROPERTIES			0x80 	/**< Additional characteristic properties are defined in the Characteristic Extended Properties descriptor */


/*==================[typedef]================================================*/

typedef uint16_t	UUIDstd_t;			/**< Standard 16-bit UUID.*/
typedef uint8_t		UUIDnonstd_t[16];	/**< Non-standard 128-bit UUID.*/

/**
 * @brief	Attribute value of a characteristic declaration
 */
typedef struct{
	uint8_t			gatt_char_prop;	/**< Bit field of characteristic properties. */
	uint16_t 		gatt_char_value_handle;	/**< Handle of the Attribute containing the value of this characteristic. */
	UUIDnonstd_t	gatt_char_uuid;
}gatt_att_value_t;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/**
 * @brief	Hexadecimal character to unsigned integer conversor
 *
 * @param c	Hexadecimal character
 *
 * @return	Corresponding unsigned integer
 */
uint8_t CharToInteger(char c);

/**
 * @brief	128-bit UUID creation.
 *
 * @param	string_uuid	The 128-bit (16-byte) UUID as a human readable const-string.
 *          			Format: XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
 */
//UUIDnonstd_t ReadUUID(const char* string_uuid);


#endif /* MODULES_LPC4337_M4_DRIVERS_BM_INC_BLE_H_ */
