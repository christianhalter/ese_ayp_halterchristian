#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include "string.h"

//Definición de comandos que se pueden recibir por desde la CIAA mediante UART

#define SERVERSTART 1
#define SERVERSTOP  2
#define ISCONNECTED 5
#define ADVERTISINGSTART  3
#define ADVERTISINGSTOP   4
#define CHREADVALUE  6
#define CHWRITEVALUE  7
#define CHACTUALIZEVALUE  8

BLEServer *pServer;
BLEService *pService;
BLECharacteristic *pCh_LedRojo;
BLECharacteristic *pCh_Funcion;
BLECharacteristic *pCh_Data;



bool deviceConnected = false;
const int LED = 5;


const byte numChars = 10;
char receivedChars[numChars];
char tempChars[numChars];        // temporary array for use when parsing

// variables to hold the parsed data

int cmd = 0;
int uuid = 0;
int value = 0;

boolean newData = false;


#define SERVICE_UUID                  "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
#define SERVICE_UUID_SHORT            1
#define CH_LED_ROJO_UUID              "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CH_LED_ROJO_UUID_SHORT        2
#define CH_FUNCION_UUID               "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
#define CH_FUNCION_UUID_SHORT         3
#define CH_VALOR_UUID                 "6E400004-B5A3-F393-E0A9-E50E24DCCA9E"
#define CH_VALOR_UUID_SHORT           4

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

class pCh_LedRojo_Callbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();

      if (rxValue.length() > 0) {
        Serial.print("<");
        Serial.print(CHACTUALIZEVALUE);
        Serial.print(",");
        Serial.print(CH_LED_ROJO_UUID_SHORT);
        Serial.print(",");
        for (int i = 0; i < rxValue.length(); i++) {
          Serial.print(rxValue[i]);
        }

        Serial.println(">");
      }
    }
};

class pCh_Funcion_Callbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();

      if (rxValue.length() > 0) {
        Serial.print("<");
        Serial.print(CHACTUALIZEVALUE);
        Serial.print(",");
        Serial.print(CH_FUNCION_UUID_SHORT);
        Serial.print(",");
        for (int i = 0; i < rxValue.length(); i++) {
          Serial.print(rxValue[i]);
        }

        Serial.println(">");
      }
    }
};

void ServerSart(){
  pService->start();
};

void ServerStop(){
  pService->stop();
};

void AdvertisingStart(){
  pServer->getAdvertising()->start();
};

void AdvertisingStop(){
  pServer->getAdvertising()->stop();
};

void IsConnected(){
  Serial.print("<");
  Serial.print(ISCONNECTED);
  Serial.print(",");
  Serial.print(0);
  Serial.print(",");
  Serial.print(deviceConnected);
  Serial.println(">");
};

void ReadCharacteristicValue(char short_UUID){
  std::string rxValue;
  switch(short_UUID){
    case 2: rxValue = pCh_LedRojo->getValue(); break;
    case 3: rxValue = pCh_Funcion->getValue(); break;
    case 4: rxValue = pCh_Data->getValue(); break;
    default: break;
  };
  
  if (rxValue.length() > 0) {
        Serial.print("<");
        Serial.print(CHACTUALIZEVALUE);
        Serial.print(",");
        Serial.print(short_UUID,DEC);
        Serial.print(",");

        for (int i = 0; i < rxValue.length(); i++) {
          Serial.print(rxValue[i]);
        }
    
        Serial.println(">");
      };
  
};

void WriteCharacteristicValue(char short_UUID, int valor){
  switch(short_UUID){
    case 2: pCh_LedRojo->setValue(valor); pCh_LedRojo->notify(); break;
    case 3: pCh_Funcion->setValue(valor); break; //no es necesaria
    case 4: pCh_Data->setValue(valor); pCh_Data->notify(); break;
    default: break;
  };
}


void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);

  BLEDevice::init("ESE-ESP32BLE");
  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic for handle Red Led on CIIA
  pCh_LedRojo = pService->createCharacteristic(
                      CH_LED_ROJO_UUID, 
                      BLECharacteristic::PROPERTY_READ |
                      BLECharacteristic::PROPERTY_WRITE |
                      BLECharacteristic::PROPERTY_NOTIFY
                    );
                      
  pCh_LedRojo->addDescriptor(new BLE2902());
  pCh_LedRojo->setCallbacks(new pCh_LedRojo_Callbacks());
  pCh_LedRojo->setValue("0");

   // Create a BLE Characteristic for execute funtions on CIIA
   pCh_Funcion = pService->createCharacteristic(
                      CH_FUNCION_UUID, 
                      BLECharacteristic::PROPERTY_READ |
                      BLECharacteristic::PROPERTY_WRITE 
                    );
                      
  pCh_Funcion->addDescriptor(new BLE2902());
  pCh_Funcion->setCallbacks(new pCh_Funcion_Callbacks());
  pCh_Funcion->setValue("0");

  // Create a BLE Characteristic for advertising data from CIIA
   pCh_Data = pService->createCharacteristic(
                      CH_VALOR_UUID, 
                      BLECharacteristic::PROPERTY_READ |
                      BLECharacteristic::PROPERTY_NOTIFY 
                    );
                      
  pCh_Data->addDescriptor(new BLE2902());
  //pCh_Data->setCallbacks(new pCh_Data_Callbacks()); //not necesary
  pCh_Data->setValue("0");

  
}

void loop() {
  
while (Serial.available()){
    recvWithStartEndMarkers();
    if (newData == true) {
        strcpy(tempChars, receivedChars);
            // this temporary copy is necessary to protect the original data
            //   because strtok() used in parseData() replaces the commas with \0
        parseData();
       // showParsedData();
        newData = false;
        executeCmd(cmd,uuid,value);
    }
}
}
//============
void executeCmd(int c, int id, int v)
{
      switch(c){
      case SERVERSTART: ServerSart(); break; 
      case SERVERSTOP: ServerStop(); break; 
      case ADVERTISINGSTART: AdvertisingStart(); break; 
      case ADVERTISINGSTOP: AdvertisingStop();break; 
      case ISCONNECTED: IsConnected(); break; 
      case CHREADVALUE: ReadCharacteristicValue(id); break;
      case CHWRITEVALUE: WriteCharacteristicValue(id,v); break;
    };
}

void recvWithStartEndMarkers() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;

    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }

        else if (rc == startMarker) {
            recvInProgress = true;
        }
    }
}

//============

void parseData() {      // split the data into its parts

    char * strtokIndx; // this is used by strtok() as an index

    strtokIndx = strtok(tempChars,",");      // get the first part - the string
    //strcpy(cmd, strtokIndx);
    cmd = atoi(strtokIndx); // copy it to messageFromPC
 
    strtokIndx = strtok(NULL, ","); // this continues where the previous call left off
    //strcpy(uuid, strtokIndx);
    uuid = atoi(strtokIndx);     // convert this part to an integer

    strtokIndx = strtok(NULL, ",");
    //strcpy(value, strtokIndx);
    value = atoi(strtokIndx);

//    strtokIndx = strtok(NULL, ",");
//    int decena = atoi(strtokIndx);
//
//    strtokIndx = strtok(NULL, ",");
//    int unidad = atoi(strtokIndx);
//    
//    value = centena*100 + decena*10 + unidad;     // convert this part to a float

}

//============

void showParsedData() {
    Serial.print("CMD ");
    Serial.println(cmd);
    Serial.print("UUID ");
    Serial.println(uuid);
    Serial.print("Valor ");
    Serial.println(value);
}
